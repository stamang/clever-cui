# Tagger execution - Suzanne Tamang 2020-08-02

# tagger expecution, you will need to create the output directy
python2 src/step1_tagger.py --lexicon res/termSNOMED.txt --section-headers res/headers.txt --snippet-length 125 --snippets --notes notes/testnotes_formatted.txt --workers 16 --output output/snomed --left-gram-context 3 --right-gram-context 2

# weakLabeler execution
python src/step2_weakLabel.py -s STD --lexicon res/termSNOMED.txt --note-metadata notes/testnotemdata.txt --input output/snomed --label-output output/weakLabels
