# Tagger execution - by Suzanne Tamang; 2020-08-02
python /share/pi/stamang/people/stam/clever-cui/src/step1_tagger.py --lexicon /share/pi/stamang/people/stam/clever-cui/res/termSNOMED.txt --section-headers /share/pi/stamang/people/stam/clever-cui/res/headers.txt --snippet-length 125 --snippets --notes /share/pi/stamang/people/stam/clever-cui/notes/testnotes_formatted.txt --workers 16 --output /share/pi/stamang/people/stam/clever-cui/output/ants --left-gram-context 3 --right-gram-context 2

# weakLabeler execution
python /share/pi/stamang/people/stam/clever-cui/src/step2_weakLabel.py -s STD --lexicon /share/pi/stamang/people/stam/clever-cui/res/termSNOMED.txt --note-metadata /share/pi/stamang/people/stam/clever-cui/notes/testnotemdata.txt --input /share/pi/stamang/people/stam/clever-cui/output/ants --label-output /share/pi/stamang/people/stam/clever-cui/output/weakLabels
