# clever-cui

CLEVER code and associated resources for all clinical concept extraction

=====
SCRIPT
clever-cui.sh includes a script to run clever's tagger and weak labeling scripts.

=====
DEMO SETUP
src: 	  includes the python scripts and the associated libraries for trainng embeddings, the tagger and the weak lableing step.
res: 	  this includes the terminology and the header resources
notes:	  this inclues the MIMIC3 notes and assocaited metadata
output:	  this is the output directory for the annotation and weak labeling output

=====
CLEVER output Field dictionary
2020-07-12

Introduction
This document is a dictionary of field definitions for each of the fields and sub-fields for the CLEVER "All Concepts" output files.

The documentation will describe the fields in each of three types of output files:

•	extraction - This refers to any file in the directory "output/.../extraction-NNNNNNN.tsv (where "<run name>" is specified in the command line, so that you can store multiple runs in the output directory, and "NNNNNNN" is an integer indicating which thread the output came from).  These files are intermediate output from the routine "src/step1_tagger.py" and they provide input for the last routine, "src/step2_weakLabel.py".  You may want to study this dictionary if you need help understanding  the next two final output tables.
•	allPos - This refers to the single output file: "output/weakLabels/allPos.txt".  This file contains one line for each concept identified in a clinical note that was not found to be "Non-positive" by a modifier term such as "B-FAM" for family history, or 'NEGEX' for a modifier phrase indicating that the concept was actively confirmed to be not true for the patient.  All the "Non-Positive" concepts are in the next file type, allNonPos.
•	allNonPos - This refers to the single output file: "output/weakLabels/allNonPos.txt".  These are concepts that were identified, but were associated with a modifier concept that suggested that the concept did not really take place for this patient.  For instance, if the patient has a family history of cancer, then this file will mention "cancer" as a concept, but also "FAM" as a modifier term that indicates that it was probably not the patient who had cancer, but someone else in his/her family.  The format of this "allNonPos.txt" file is a bit more complicated than the "allPos.txt" file, so it will be explained last after the "allPos.txt" file is explained.
Note on structure of termSNOMED.txt
This table has three fields:
•	sequence number - this is a unique row identifier. 
•	concept term or synonym - This is a string that is used as a search term of the clinical note for a given concept.  There may be more than one row for the same concept ID (3rd field), where the concept term is different.  This concept is matched if any one of these terms is found in the clinical note.  In the case of the SNOMED CUI's (third field does not begin with "B-"), the first term for a given CUI is the "Preferred term" for that CUI and the subsequent terms (if any) are synonyms for that CUI.
•	Concept ID - this is a CUI (eg C0028678) or a modifier concept ID (B-NEG, B-FAM, etc).  
extraction fields
These files are produced by "src/step1_tagger.py" along with other files ("context-left-NNNNNNN.tsv", "context-right-NNNNNNN.tsv" and "discover-NNNNNNN.tsv").  The other three output file types will not be documented because they are not used by the script (step2_weakLabel.py) that produces the final output, and are therefore not critical to understanding the final output.

The "extraction" files are named with the pattern: "output/ants/<run name>/extraction-NNNNNNN.tsv" where 'NNNNNNN' is the thread ID that produced this output.

Each record or row in the file represents one Target UMLS concept CUI (from the file "termSNOMED.txt" where the 3rd field does not start with "B-") along with possible additional CUI's in one clinical note for a patient.  In addition, the record may contain one or more modifier concept ID's that begin with "B-", that are also found in the termSNOMED.txt file.

Below is an example record from the extraction file "extraction-3028524.tsv", record 10:

3	C0042789:136956	65		C0028678:93530:0:-65	B-PT:8:21:-44	C0184666:214829:24:-41	B-SCREEN:729:52:-13	B-DOT:2:56:-9	B-PUNCT:6:173:108	Nursing Transfer notePt admitted to NICU for sepsis eval. Please see Attendingnote for details regarding maternal history and deliverydetails.Infant stable in RA. RR 30-40's, sats 96-100%. LS c

If you are viewing this document in MS Word, you will note that the major fields are delimited by a tab character, " ", and the sub-fields are delimited by a ":".  The fields and sub-fields will now be defined below.  For each field, it will give a name followed by the data value in parentheses from the sample record above.

•	mimic_id (3) - This is the note ID.  It is unique for each note.  It is the second field in the MIMIC notes metadata file, "notes/testnotemdata.txt"
•	Target_CUI_info (C0042789:136956) - This is information about the "target" CUI which is the primary CUI for this record against which other tags are measured.  It has two sub-fields, separated by ":"
o	target_CUI (C0042789) - this is the target concept unique identifier (or other concept identifier if not using UMLS) that was tagged.  This value corresponds to the 3rd field in the dictionary, termSNOMED.txt, and it must be a value that does not begin with "B-", because those concepts are "modifier" concepts.  Note that a given concept has one or more strings (the second field in termSNOMED.txt) associated with it, and any of these strings could have matched the note text in the snippet (described below) for this concept to get identified. For the sake of brevity, the matching term is not noted in the record, but we can find it with the offset described below.
o	target_term_ID (136956) - This is a unique row ID in file "termSNOMED.txt", and it matches the first field in that file.  If we open the file with an editor, we can search for "136956" and find the line
136956|see|C0042789
This tells us that the string "see" was found in the clinical note and associated with this target_CUI "C0042789".  If you go to the UMLS Metathesaurus browser and enter this CUI, you will find that the concept is described by the word "Vision" and "see" is one of the atomic terms (A18687505) associated with that CUI. If you look at the snippet in our example record, you will see that the word "see" is in the middle of the snippet:  "…Please see Attendingnote for details…"
•	Target_offset (65) - This is the character offset, within the snippet, of the first character where the target string "see" begins.  If you look at the snippet that begins with "Nursing transfer…", and you treat the first character as "1" and add 65 characters, you will be at the "s" in the word "see".
•	header_fields () - If a section header is captured, then this field contains two sub-fields separated by ':':
o	header - the header name
o	hpos - the character offset of the header in the document.
In the MIMIC3 notes we found no places where this was captured, and the header_fields was empty.
•	Additional Concepts - next are a variable number of additional concepts, in the same format as the "target concept" above, that have been tagged in this record.  In our example record, the additional concepts are: 
(C0028678:93530:0:-65)
(B-PT:8:21:-44)
(C0184666:214829:24:-41)
(B-SCREEN:729:52:-13)
(B-DOT:2:56:-9)
(B-PUNCT:6:173:108)
If we want to parse this file, we know that there are exactly 6 of these "Additional Concepts" fields, because these are the last fields before the final field "Snippet".  We take the number of major fields in this record and subtract one field for Snippet, and that is where the last "Additional Concepts" field is located.
As you can see, each of the additional concepts has 4 sub-fields, delimited by ":".  The subfields are
•	Concept ID - as shown above, this can be either a CUI (C0028678) or a modifier concept (B-PT, B-SCREEN, B-DOT, B-PUNCT).  The modifier concepts all begin with "B-" and are interpreted as possibly modifying the meaning of the target concept. How each of these concepts is handled by clever is determined by the python code in "src / step2_weakLabelRuleFcns.py" in function "cleverRule()".  For instance, the modifier concepts (or classes of concepts) NEGEX, HYP, FAM, HX, and SCREEN are used to make a concept "Non positive" in this line of code:
o	supress = [NEGEX,HYP,FAM,HX,SCREEN]
•	term ID - this is the ID of the term, which matches the first field of one of the rows in "res/termSNOMED.txt".  In the case of "93530" above, termSNOMED.txt identifies the term to be "nursing", which is a synonym for the preferred term of "Nursing Therapy".  It also verifies that the CUI for that term is "C028678", which we already knew.
•	offset - this is the character offset, within the snippet, where the beginning of the concept term appears.  IN the first concept (C0028678 - "nursing"), the offset is 0, and as we can see, the first word of the snippet is "Nursing".  In the second concept (B-PT - "pt"), the offset is 21, and the at this offset "Pt admitted" is found.
•	offset from Target - This is the number of bytes (positive or negative) that the term is offset from the target term (C0042789 - "see").  In the first addition concept (C0028678 - "nursing") we can see that "-65" means that the beginning of "nursing" appears 65 characters to the left of the term "see".
Snippet (Nursing Transfer notePt admitted to NICU for sepsis eval. Please see Attendingnote for details regarding maternal history and deliverydetails.Infant stable in RA. RR 30-40's, sats 96-100%. LS c)   - This is the snippet of text that shows you the context of all the concepts and modifiers that were identified in this record).  The length of the snippet is controlled by the parameter "-snippet-length 125" on the command line when you executed "step1_tagger.py".  This means that the snippet contains up to 125 characters before the target term "see" and up to 125 characters after the end of the target term "see".  In this case, the snippet has only 65 characters before "see" because that is the beginning of the note.

allPos fields
This file is produced by "src/step2_weakLabel.py".  It is created with the pathname: "output/labels/allPos.txt". It contains all the clinical concepts that have been extracted, but have not been determined to be "Non positive".

Each record or row in the file represents one target UMLS concept CUI (from the file "termSNOMED.txt" where the 3rd field does not begin with "B-"). Within the record, the fields are separated by the pipe character "|", and not the tab character as in the "extraction" files.  Within the fields there may be sub-fields with various delimiters which will be described for each field below.

Below is an example record from the allPos file "allPos_2020-06-24.txt", record or line number 5:

[POSITIVE,C0011209]|S-5-C0011209|C0028678_#C0011209#_B-DOT_B-PT_B-PT|C0028678_#C0011209#_B-DOT_B-PT_B-PT|delivery|4|103|"Nursing/Other"|2579-04-09 06:18:00 EST|XXXX|C0011209|33653|84|:|nursing:C0028678:93530:5:-79|period:B-DOT:2:105:21|patient:B-PT:9:116:32|patient:B-PT:9:182:98|SNIPPET: NICU Nursing Septic Workup Note[**Name8 (MD) 7**] NNP note for maternal history and delivery room details. [**Known patient lastname **] NICU from L&D for septic workup due [**Known patient lastname **] maternal temp

For each field, the list below will give the name of the field, followed by the data value in parentheses from the sample record above.

•	Target concept ([POSITIVE,C0011209]) - this is the target concept term for this record, preceded by the word "POSITIVE".  The word "POSITIVE" is always there because the file is "all positive" concepts; in the "allNonPos" file described next, each record will have the word "NEGATIVE".  The entire field is surrounded by square brackets, and the sub-fields below are separated by a "," character.
o	Positive or negative concept (POSITIVE).  This flags that the concept found was a positive assertion of the presence of that concept in the patient.  In a clinical note, providers are trained to ask questions that are relevant to the patient's presenting complaint, and to document all positive findings as well as all "significant negatives".  For example, if a patient is coughing, it is important for the provider to ask whether they have chest pain and to document the answer in the note, even if the answer is "no", because this shows that they have investigated the possibility that the patient may have (painful) pleural inflammation.  For this reason, it is critical that we annotate all negative assertions of concepts such as "chest pain".
o	Concept ID (C0011209) - this will be the target CUI (in the case of termSNOMED.txt) or whatever concept ID is in the third field for the matching term in the term dictionary we are using.
•	Target synopsis (S-5-C0011209) - This is a synopsis of three sub-fields of the target concept, delimited by the "-" character.  The three sub-fields are:
o	constant_s (S) - this is hard-coded in the source code; don't know why.
o	patient_id (5) - this is the patient ID found in the corpus.  It is the third field of the notes metadata file "testnotemdata.txt".
o	targ_CUI (C0011209) - the target CUI
•	fullseq (C0028678_#C0011209#_B-DOT_B-PT_B-PT) - This is the full summary sequence of concepts found in the snippet.  It is a list of subfields delimited by the "_", where each of the sub-fields is a concept ID.  The number of concepts is variable and the sequence is in the order found in the snippet.  The concept surrounded by "#" characters is the target concept.  In this example, the concept ID's can be interpreted as:
o	seq_concept[0] (C0028678) - This CUI ("nursing therapy") that matched on the term "nursing"
o	seq_concept[1] (#C0011209#) - this is the target concept "Obstetric deliver" that matched on the term "delivery" 
o	seq_concept[2] (B-DOT) - this is the modifier concept that matched on the period (".") character (after "details" in the snippet)
o	seq_concept[3] (B-PT) - this is the modifier concept that matched on the term "patient".
o	seq_concept[4] (B-PT) - this is the second occurrence of the same modifier concept, that matched on the second occurrence of "patient"
•	truncseq (C0028678_#C0011209#_B-DOT_B-PT_B-PT) - This is the possibly truncated summary sequence of concept found in the snippet.  For instance, if a given concept comes more than 125 characters before the target concept then it will be omitted from the truncseq but maybe not from the fullseq. 
•	target_concept_term (delivery) - this is the term that the target concept found in the snippet
•	patient_id (4) - This is the patient ID that we assigned to the patient in preparing the metadata.  It is the first field in "testnotemdata.txt"
•	note_id (103) - This is the note ID that we assigned.  It is the second field in "testnotemdata.txt" and the first field in "notes_formatted.txt".  We use this field to relate the note text to the note metadata and to this output file.
•	note_type ("Nursing/Other") - This is the type of note, and is found in field 5 in the file "testnotemdata.txt".
•	note_timestamp (2579-04-09 06:18:00 EST) - This is the note timestamp that is found in field 4 in the file "testnotemdata.txt"
•	nyear (XXXX) - this was always 'XXXX' in the Mimic3 data
•	target_concept_id (C0011209) - this is the ID of the target concept ("Obstetric delivery") that matched on the synonym "delivery".
•	target_term_id (33653) - this is the unique id and first column of the file "termSNOMED.txt" that shows the concept ID (C0011209) and the associated term that matched the note (delivery).
•	target_term_offset (84) - This is the character offset into the snippet where the target term "deliver" was found.
•	head (UK) - This is the section header name, if found.  If not found, defaults to 'UK'.  All records had 'UK' here in the MIMIC3 data.
•	hpos (NULL) - This is the byte offset of the section header.  If no section header found, it defaults to 'NULL'.  All records had 'NULL' here in the MIMIC3 data.
•	other_concept[0] (nursing:C0028678:93530:5:-79) - The array "other_concept" has N-1 values, where 'N' is the number of concepts in fullseq above.  Each element of this array contains a series of 5 sub-fields, delimited by ":", for each concept other than the target concept, which is described above.  We describe the sub-fields below:
o	concept_term (nursing) - this is the term that the concept matched on.
o	concept_id (C0028678) - this is the concept ID of the matching concept.  In the dictionary, we can see that the preferred term for this concept is "nursing therapy", though the concept matched on the synonym term "nursing"
o	term_id (93530) - this is the value of field 1,  the unique row ID, in file "termSNOMED.txt" where we find the term "nursing" and the concept ID (C0028678).
o	offset (5) - This is the number of characters from the beginning of the snippet where the term "nursing" is found.
o	offset_from_target (-79) - this is the character offset of the other concept term "nursing" from the target term "delivery" in the snippet.  The negative number means that you count 79 characters to the left of the target term to find the current concept "nursing".
•	other_concept[1] (period:B-DOT:2:105:21) - This is the next "other concept".  The fields are structure the same for each of these other concepts.  In this case, the concept is a "modifier" concept, "B-DOT", rather than a CUI clinical concept.  This concept is a period (".") and is probably used to mark the boundary between two sentences.  That is important because a modifier to a target concept must be in the same sentence as the target concept.
•	other_concept[2] (patient:B-PT:9:116:32) - same format as above.
•	other_concept[3] (patient:B-PT:9:182:98) - same format as above.
•	snippet (SNIPPET: NICU Nursing Septic Workup Note[**Name8 (MD) 7**] NNP note for maternal history and delivery room details. [**Known patient lastname **] NICU from L&D for septic workup due [**Known patient lastname **] maternal temp) - This is a section of the clinical note that surrounds the target term ("delivery").  The size of the snippet is controlled by a command line parameter, "--snippet-length 125" when executing the first step script "step1_tagger.py". This means that the snippet will include up to 125 characters before the beginning of the target term "delivery" and up to 125 characters after the end of "delivery".  In this case, the snippet has only 84 characters before "delivery" because that is the beginning of the note.
allNonPos fields

